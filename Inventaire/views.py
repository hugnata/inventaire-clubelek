from django.shortcuts import render
from rest_framework import viewsets, status, filters
from rest_framework.decorators import api_view
from rest_framework.response import Response

from Inventaire.Serializers import ObjetSerializer, MeubleSerializer, EtagereSerializer
from Inventaire.models import Etagere, Meuble, Objet


# Vue qui renvoie un meuble sous format JSON à l'aide de son IDplan (ID sur le plan en SVG)
@api_view(['GET'])
def MeubleByIdView(request, pk, format=None):
    try:
        meuble = Meuble.objects.get(idplan=pk)
    except Meuble.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        context = {'request': request}
        serializer = MeubleSerializer(meuble, context=context)
        return Response(serializer.data)


# La vue pour les étagères, renvoie du JSON. Pour en savoir plus, c'est du Django REST framework (DRF)
class EtagereViewSet(viewsets.ModelViewSet):
    queryset = Etagere.objects.all()
    serializer_class = EtagereSerializer


# La vue pour les meubles, renvoie du JSON. Pour en savoir plus, c'est du Django REST framework (DRF)
class MeubleViewSet(viewsets.ModelViewSet):
    queryset = Meuble.objects.all()
    serializer_class = MeubleSerializer


# La vue pour les objets, renvoie du JSON. Pour en savoir plus, c'est du Django REST framework (DRF)
class ObjetViewSet(viewsets.ModelViewSet):
    queryset = Objet.objects.all()
    serializer_class = ObjetSerializer
    filter_backends = (filters.SearchFilter,)  # Permet de rechercher
    search_fields = ('nom', 'tag')  # Recherche par nom et tag


# Vue qui permet d'afficher l'inventaire d'un meuble en version imprimable
def meuble(request, pk):
    context = {
        'meuble': Meuble.objects.get(id=pk),
    }
    return render(request, "meuble.html", context=context)


# La page d'acceuil
def index(request):
    return render(request, "index.html")
