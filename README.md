# Inventaire du club

Le but du projet est de créer un inventaire pour le club.
Ce projet est créé en django. Il utilise django-rest-framework pour la communication avec le frontend.
Le frontend est en javascript + jquery. 

Ce projet a vocation à être modifié/amélioré par tous (en vrai pour l'instant il est moche)

Pour toutes questions, contactez hugo Reymond

## Structure du projet

Le projet suit la structure du framework django
Il se déploie comme un projet Django

## Comment utiliser l'application

- La page d'acceuil (template: index.html) montre le plan du club, on peut y cherche des objets
- La page /meubles/<id> montre le contenu du meuble dans une version imprimable pour coller contre le meuble (pour l'instant c'est tout pété, juges pas)
- La page d'administration permet d'emprunter des objets, grosso modo c'est la page django admin de base.
 