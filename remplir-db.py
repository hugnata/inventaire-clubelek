##Ce script permet de peupler la base de donnée avec des valeurs par défaut.

# Setup de l'environnement
import os

print("Peuplement de la base de donnée...")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "InventaireClub.settings")
import django

django.setup()
from django.contrib.auth.models import User

# Import des modules
from Inventaire import models

print("Nettoyage de la BDD...", end="", flush="true")
models.Objet.objects.all().delete()
models.Etagere.objects.all().delete()
models.Meuble.objects.all().delete()
print("OK")

print("Création des meubles...", end="", flush="true")
# Création des meubles
etagereif = models.Meuble.objects.create(nom="Armoire IF", idplan="armoire_if")
bibli = models.Meuble.objects.create(nom="Bibliothèque", idplan="armoire_entree")
models.Meuble.objects.create(nom="Meuble entrée", idplan="meuble_entree")
vitrine = models.Meuble.objects.create(nom="Vitrine", idplan="vitrine")
sac = models.Meuble.objects.create(nom="Sac", idplan="sac")
casier1 = models.Meuble.objects.create(nom="Casiers 1", idplan="casier1")
casier2 = models.Meuble.objects.create(nom="Casiers 2", idplan="casier2")
pisto = models.Meuble.objects.create(nom="Meuble Pisto-colle", idplan="pistocolle")
meca = models.Meuble.objects.create(nom="Etabli méca", idplan="etabli_meca")
elec = models.Meuble.objects.create(nom="Etabli Elec", idplan="etabli_elec")
cables = models.Meuble.objects.create(nom="Meuble Cables", idplan="cables")
proto = models.Meuble.objects.create(nom="Zone proto", idplan="zoneproto")
models.Meuble.objects.create(nom="Casiers 3", idplan="casier3")
table_reu = models.Meuble.objects.create(nom="Table de réunion", idplan="table_reunion")
table_coupe = models.Meuble.objects.create(nom="Table coupe de france", idplan="table_coupe")
table_tesla = models.Meuble.objects.create(nom="Table bobine Tesla", idplan="table_tesla")
frigo = models.Meuble.objects.create(nom="Frigo", idplan="frigo")
cuisine = models.Meuble.objects.create(nom="Armoire cuisine", idplan="armoire_cuisine")
reserve = models.Meuble.objects.create(nom="Réserve", idplan="reserve")
print("OK")

print("Création des étagères", end="", flush="true")
# Création des étagères de l'armoire IF
eta_if_1 = models.Etagere.objects.create(meuble=etagereif, emplacement="1ère Etagère")
eta_if_2 = models.Etagere.objects.create(meuble=etagereif, emplacement="2ème Etagère")
eta_if_3 = models.Etagere.objects.create(meuble=etagereif, emplacement="3ème Etagère")
eta_if_4 = models.Etagere.objects.create(meuble=etagereif, emplacement="4ème Etagère")
models.Etagere.objects.create(meuble=etagereif, emplacement="5ème Etagère")
models.Etagere.objects.create(meuble=etagereif, emplacement="6ème Etagère")

# Création des étagère biblio
models.Etagere.objects.create(meuble=bibli, emplacement="1ère Etagère")
models.Etagere.objects.create(meuble=bibli, emplacement="2ème Etagère")
models.Etagere.objects.create(meuble=bibli, emplacement="3ème Etagère")
models.Etagere.objects.create(meuble=bibli, emplacement="4ème Etagère")
models.Etagere.objects.create(meuble=bibli, emplacement="5ème Etagère")

# Création des étagère meuble entrée
# FLEMME

# Création des étagère Vitrine
models.Etagere.objects.create(meuble=vitrine, emplacement="Toit")
models.Etagere.objects.create(meuble=vitrine, emplacement="1ère Etagère")
models.Etagere.objects.create(meuble=vitrine, emplacement="2ème Etagère")
models.Etagere.objects.create(meuble=vitrine, emplacement="3ème Etagère")
models.Etagere.objects.create(meuble=vitrine, emplacement="4ème Etagère")

print(".", end="", flush="true")
# Création des étagère Pistocolle
models.Etagere.objects.create(meuble=pisto, emplacement="Tiroir gauche")
models.Etagere.objects.create(meuble=pisto, emplacement="Tiroir droite")
models.Etagere.objects.create(meuble=pisto, emplacement="1ère Etagère")
models.Etagere.objects.create(meuble=pisto, emplacement="2ème Etagère")

# Création des etagères meuble méca
models.Etagere.objects.create(meuble=meca, emplacement="Dessus")
models.Etagere.objects.create(meuble=meca, emplacement="Gauche")
models.Etagere.objects.create(meuble=meca, emplacement="Centre")
models.Etagere.objects.create(meuble=meca, emplacement="Droite")

# Création des etagères meuble elec
models.Etagere.objects.create(meuble=elec, emplacement="Dessus")
models.Etagere.objects.create(meuble=elec, emplacement="Gauche")
models.Etagere.objects.create(meuble=elec, emplacement="Centre")
models.Etagere.objects.create(meuble=elec, emplacement="Droite")
models.Etagere.objects.create(meuble=elec, emplacement="Sous l'évier")

print(".", end="", flush="true")

# Création des etagères meuble cables
models.Etagere.objects.create(meuble=cables, emplacement="1ère Etagère")
models.Etagere.objects.create(meuble=cables, emplacement="2ème Etagère")
models.Etagere.objects.create(meuble=cables, emplacement="3ème Etagère")
models.Etagere.objects.create(meuble=cables, emplacement="4ème Etagère")
models.Etagere.objects.create(meuble=cables, emplacement="5ème Etagère")
models.Etagere.objects.create(meuble=cables, emplacement="6ème Etagère")

# Création Zone Proto
models.Etagere.objects.create(meuble=proto, emplacement="Etagère du dessus")

# Création frigo
models.Etagere.objects.create(meuble=frigo, emplacement="Frigo")

# Création cuisine
models.Etagere.objects.create(meuble=cuisine, emplacement="Consommables")

print(".", end="", flush="true")

# Création réserve
models.Etagere.objects.create(meuble=reserve, emplacement="1ère Etagère")
models.Etagere.objects.create(meuble=reserve, emplacement="2ème Etagère")
models.Etagere.objects.create(meuble=reserve, emplacement="3ème Etagère")
models.Etagere.objects.create(meuble=reserve, emplacement="4ème Etagère")
models.Etagere.objects.create(meuble=reserve, emplacement="5ème Etagère")

# Création du bas des tables
models.Etagere.objects.create(meuble=table_reu, emplacement="Dessous")
models.Etagere.objects.create(meuble=table_coupe, emplacement="Dessous")
models.Etagere.objects.create(meuble=table_tesla, emplacement="Dessous")
print("OK")

print("Création des objets...", end=" ")

# Création des objets de l'armoire IF
print("OK")

# Création du superuser
print("Création du superuser...", end="", flush="true")
if (User.objects.filter(username='clubelek').count() == 0):
    User.objects.create_superuser('clubelek', 'clubelek@clubelek.fr', 'admin')
print("OK")
