from rest_framework import serializers
from rest_framework.fields import CharField

from Inventaire.models import Objet, Meuble, Etagere


# CONTIENT TOUT LES SERIALIZERS (permettent de transformer des objets en JSON). Pour en savoir plus, DJANGO REST FRAMEWORK

class ObjetSerializer(serializers.HyperlinkedModelSerializer):
    emplacement = CharField(source="etagere.emplacement")
    meuble = CharField(source="etagere.meuble.nom")
    meuble_idplan = CharField(source="etagere.meuble.idplan")

    class Meta:
        model = Objet
        fields = ("id", "nom", "type", "tag", "a_donner", "a_reparer", "emplacement", "meuble", "meuble_idplan")


class EtagereSerializer(serializers.HyperlinkedModelSerializer):
    objets = ObjetSerializer(many=True, read_only=True)

    class Meta:
        model = Etagere
        fields = ("id", "emplacement", "objets")


class MeubleSerializer(serializers.HyperlinkedModelSerializer):
    etageres = EtagereSerializer(many=True, read_only=True)

    class Meta:
        model = Meuble
        fields = ("id", "nom", "etageres");
        depth = 2
