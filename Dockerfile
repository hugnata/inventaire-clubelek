FROM python:3

ENV PROD True

RUN mkdir /code

WORKDIR /code

COPY requirements.txt /code/

RUN pip install -r requirements.txt

COPY . /code/

RUN chmod +x run-prod.sh

EXPOSE 80

CMD ./run-prod.sh
