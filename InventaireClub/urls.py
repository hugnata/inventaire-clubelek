"""InventaireClub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from Inventaire import views
from Inventaire.views import ObjetViewSet, MeubleViewSet, EtagereViewSet

router = routers.DefaultRouter()

router.register(r'objets', ObjetViewSet)
router.register(r'meubles', MeubleViewSet)
router.register(r'etageres', EtagereViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),  # Interface d'admin
    path('meuble-by-idplan/<pk>', views.MeubleByIdView),
    # Renvoie un meuble selon son idplan (ID sur le plan du club en SVG)
    url(r'^$', views.index),  # La page d'acceuil
    url(r'^', include(router.urls)),  # Les routes de DJango Rest Framework (DRF)
    path('meubles-export/<int:pk>', views.meuble),  # La page d'affichage du contenu des meubles

]
