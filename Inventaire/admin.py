from django.contrib import admin

from Inventaire.models import Objet, Etagere, Meuble, Membre, Emprunt


# Ici les paramètres de l'interface d'administration

class EtagereInline(admin.TabularInline):
    model = Etagere


class ObjetInline(admin.TabularInline):
    model = Objet


@admin.register(Etagere)
class EtagereAdmin(admin.ModelAdmin):
    inlines = (ObjetInline,)


@admin.register(Meuble)
class MeubleAdmin(admin.ModelAdmin):
    inlines = (EtagereInline,)


@admin.register(Objet)
class ObjetAdmin(admin.ModelAdmin):
    empty_value_display = "??"
    list_display = ('nom', 'quantite', 'a_donner', 'a_reparer', 'etagere')
    list_filter = ('a_donner', 'a_reparer', 'etagere__meuble')
    search_fields = ('nom', 'tag')


@admin.register(Emprunt)
class EmpruntAdmin(admin.ModelAdmin):
    raw_id_fields = ("quoi",)
# Register your models here.
admin.site.register(Membre)
