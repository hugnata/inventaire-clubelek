var isblinking;
var whoisblinking;
var intervalblinking;

//Démarrage de la page
jQuery(document).ready(function () {
    console.log("Démarrage du document");
    afficherAide();
});

//On ajoute des events listener sur les rectangles qui composent le plan
$(document).ready(function () {
        $("#planclub").children().filter('rect').click(function () {
            $.ajax({
                url: "meuble-by-idplan/" + this.id,
                type: "GET",
                dataType: "json",
                success: afficherMeuble,
                error: afficherAide
            });
        });
        $("#recherche").keyup(function (event){
            if (event.keyCode === 13) {
                event.preventDefault();
                $("#recherche").next("button")[0].click();
            }
        });
    }
);

//Fonction appelée lors de la recherche
function recherche() {
    var mot = $('#recherche');
    console.log("Recherche en cours " + mot[0].value);
    $.ajax({
        url: "objets/?search=" + mot[0].value,
        type: "GET",
        dataType: "json",
        success: afficherResultatRecherche,
        error: function () {
            afficherMessage("Une erreur est advenue...", "La requète de recherche a échoué... Peut être le serveur est t-il down ?")
        }
    });
}

//Fonction qui met en page les résultats de la recherche
function afficherResultatRecherche(resultat, status) {

    if (resultat.length > 0) {
        var div = $("<div class='etagere'></div>");
        div.append("<h2>Résultats de la recherche</h2>");
        for (var i = 0; i < resultat.length; i++) {
            var objet = resultat[i];
            //On crée une nouvelle div qui contient l'objet
            var ligne = $("<div class=" + objet.type + " ></div>");
            var emplacement = "<span style='font-weight: lighter;'>   " + objet.meuble + "  -  " + objet.emplacement + "</span>";
            var tags = ""; //Les tags qui sont ajoutés (a donner, a reparer)
            if (objet.a_donner) {
                tags += "<span class=\"tag\">A donner</span>";
            }
            if (objet.a_reparer) {
                tags += "<span class=\"tag\">A réparer</span>";
            }
            var action = "<a href='admin/Inventaire/emprunt/add/?quoi=" + objet.id + "'  style='float: right;'>emprunter</a> ";

            ligne.append("<p><span style='float:left'>" + objet.nom + "</span>" + emplacement + tags + action + "</p>");
            ligne.hover(function () {
                return surbrillance(objet.meuble_idplan);
            }, stopblinking);

            div.append(ligne)
        }
        $("#elementachanger")[0].firstChild.replaceWith(div[0]);
    } else {
        afficherMessage("Aucun résultat", "La recherche n'a retourné aucun résultat...");
    }
}

function stopblinking() {
    clearInterval(intervalblinking);
    var rectangle = $('#' + whoisblinking);
    rectangle.css("display", "inline-block");

}

function surbrillance(rect) {
    if (isblinking) {
        stopblinking();
    }
    isblinking = true;
    whoisblinking = rect;
    var rectangle = $('#' + rect);
    intervalblinking = setInterval(function () {
        rectangle.css("display", function () {
            this.switch = !this.switch;
            return this.switch ? 'none' : 'inline-block';
        })
    }, 350);
}


function afficherMessage(titre, message) {
    var nouveau = $('<div></div>');
    nouveau.append("<h2 style='text-align: center'>" + titre + "</h2>");
    nouveau.append("<p>" + message + "</p>");
    $("#elementachanger")[0].firstChild.replaceWith(nouveau[0]);
}


//Affiche l'aide dans la div "element a changer"
function afficherAide() {
    var nouveau = $('<div></div>');
    nouveau.append("<h2 style='text-align: center'>Comment ça marche ?</h2>");
    nouveau.append("<p>Sur la gauche vous avez un magnifiiiique plan du club. " +
        "Lorsque vous cliquez sur les meubles, çela affiche leur contenu.</p>" +
        "<p>L'application d'inventaire est séparée en 2 parties:" +
        "<ul><li><b>Partie plan</b>, que vous voyez</li><li><b>Partie Administration</b>, pour emprunter des objets, les supprimer, créer son statut de membre</li></ul></p>" +
        "<p>Pour enregistrer un objet dans l'inventaire, emprunter quelque chose, ou consulter les objets à reparer, demandez à quelqu'un du bureau qu'il vous explique plus en détail</p>"
    );
    $("#elementachanger")[0].firstChild.replaceWith(nouveau[0]);

}

//Affiche un meuble dans le div "elementachanger"
function afficherMeuble(meuble) {
    var nouveau = $('<div class="meuble"></div>');
    nouveau.css('text-align', 'center');
    nouveau.append("<h2>" + meuble.nom + "</h2>");
    for (var i = 0; i < meuble.etageres.length; i++) {
        var etagere = meuble.etageres[i];
        var divetagere = $('<div class="etagere"></div>');
        divetagere.append("<h3>" + etagere.emplacement + "</h3>");
        divetagere.append("<a href=\"admin/Inventaire/objet/add/?etagere=" + etagere.id + "\">Ajouter un objet</a>");
        divetagere.append("</br>");
        for (var indice in etagere.objets) {
            var objet = etagere.objets[indice];
            //On crée une nouvelle div qui contient l'objet
            var ligne = $("<div class=" + objet.type + " ></div>");
            var tags = ""; //Les tags qui sont ajoutés (a donner, a reparer)
            if (objet.a_donner) {
                tags += "<span class=\"tag\">A donner</span>";
            }
            if (objet.a_reparer) {
                tags += "<span class=\"tag\">A réparer</span>";
            }
            var action = "<a href='admin/Inventaire/emprunt/add/?quoi=" + objet.id + "' style='float: right;'>emprunter</a> ";

            ligne.append("<p>" + objet.nom + tags + action + "</p>");
            ligne.hover(function () {
                return surbrillance(objet.meuble_idplan);
            }, stopblinking);
            divetagere.append(ligne)
        }
        nouveau.append(divetagere);
    }
    $("#elementachanger")[0].firstChild.replaceWith(nouveau[0])
}