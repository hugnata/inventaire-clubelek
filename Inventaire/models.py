from django.db import models


# Create your models here.
class Meuble(models.Model):
    nom = models.CharField("Nom du meuble", max_length=200)
    idplan = models.CharField("L'id sur le plan SVG", max_length=200)
    def __str__(self):
        return self.nom


class Etagere(models.Model):
    emplacement = models.CharField("Emplacement du casier/de l'étagère (1er = plus haut)", max_length=200,
                                   help_text="Ex: 1ère Etage/Casier Gauche")
    meuble = models.ForeignKey("Meuble", on_delete=models.CASCADE, help_text="Meuble qui contient l'étagère/Casier",
                               related_name="etageres")

    def __str__(self):
        return self.meuble.nom + " - " + self.emplacement


class Objet(models.Model):
    CHOICES = {
        ('IF', "Informatique"),
        ('ELEC', "Electronique"),
        ('MECA', "Mecanique"),
        ('DOC', "Documentation"),
        ('VIE_COURANTE', "Vie Courante"),
        ('PERRISSABLE', "Périssable"),
        ('OUTIL', "Outil"),
    }
    nom = models.CharField("Article", max_length=200)
    type = models.CharField("Le type de l'article", max_length=200, choices=CHOICES)
    tag = models.CharField("Des tags pour aider à la recherche", max_length=200, null=True, blank=True)
    quantite = models.IntegerField("Quantité", null=True, blank=True)
    a_donner = models.BooleanField("A donner", default=False)
    a_reparer = models.BooleanField("A réparer", default=False)
    etagere = models.ForeignKey("Etagere", verbose_name="Etagere/Casier", on_delete=models.CASCADE,
                                related_name="objets")

    def __str__(self):
        return self.nom + " - " + str(self.etagere) + " - " + self.type


# Model Membre, peut faire des prêts
class Membre(models.Model):
    DEPARTS = {
        ('IF', "IF"),
        ('GE', "GE"),
        ('PC', "PC"),
        ('TC', "TC"),
        ('GM', "GM"),
        ('GCU', "GCU"),
        ('BS', "BS"),
        ('GEN', "GEN"),
        ('SGM', "SGM"),
        ('BIM', "BIM"),
        ('Autre', "Autre")
    }
    nom = models.CharField("Nom", max_length=200)
    prenom = models.CharField("Prenom", max_length=200)
    depart = models.CharField("Depart", max_length=200, choices=DEPARTS)
    telephone = models.CharField("Téléphone", max_length=15)

    def __str__(self):
        return self.prenom + " " + self.nom


# Model emprunt
class Emprunt(models.Model):
    qui = models.ForeignKey("Membre", verbose_name="Qui emprunte ?", on_delete=models.CASCADE, related_name="emprunts")
    quoi = models.ForeignKey("Objet", verbose_name="Quoi ?", on_delete=models.CASCADE, related_name="pret")
    quantite = models.IntegerField("Quantité")
    date_fin = models.DateField("Jusqu'a quand ?")
    date_debut = models.DateField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.quoi.nom + " prété à " + self.qui.prenom + " " + self.qui.nom + " jusqu'à " + self.date_fin.strftime(
            "%d /%m")
